package com.vytrykhovskyi;

import com.vytrykhovskyi.planes.FreightPlane;
import com.vytrykhovskyi.planes.MilitaryPlane;
import com.vytrykhovskyi.planes.PassengerPlane;
import com.vytrykhovskyi.planes.Plane;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Application {

    protected Application() {
    }

    public static void main(final String[] args) {

        Airport airport = new Airport(planesCreator());
        firstFlight(airport.getPlanes());

        while (true) {
            System.out.println("What do u want to do?" + "\n"
                    + "0 - to sort planes by max distance" + "\n"
                    + "1 - to count seats" + "\n"
                    + "2 - to count load capacity" + "\n"
                    + "3 - to find plane by fuel using filter" + "\n"
                    + "-1 - to exit");
            switch (new Scanner(System.in).nextInt()) {
                case 0:
                    sortMaxDistance(airport);
                    break;
                case 1:
                    checkNumberOfSeats(airport);
                    break;
                case 2:
                    checkLoadCapacity(airport);
                    break;
                case 3:
                    findByFilter(airport);
                    break;
                case -1:
                    System.out.println("Bye-bye");
                    System.exit(0);
                default:
                    System.out.println("Please, repeat:");
            }
        }
    }

    private static List<Plane> planesCreator() {
        List<Plane> list = new ArrayList<>();
        list.add(new PassengerPlane("Boeing 707", 55580, 6820, 116570, 289,
                6));
        list.add(new PassengerPlane("Airbus 319", 50122, 6900, 123811, 160,
                4));
        list.add(new PassengerPlane("Airbus A300", 79600, 7540, 142000, 266,
                5));
        list.add(new MilitaryPlane("Avro 683", 13000, 4600, 30845, 7, true));
        list.add(new MilitaryPlane("Dassault Mirrage", 14500, 4240, 34512, 2,
                false));
        list.add(new FreightPlane("AN - 140", 12810, 3050, 120300, 54, 51));
        list.add(new FreightPlane("AN - 225", 34343, 15400, 250000, 76, 220));
        System.out.println("List created!!!");
        return list;
    }

    private static List<Plane> firstFlight(final List<Plane> planes) {
        System.out.println("Let them fly for first time for ... kilometers :");
        Scanner scanner = new Scanner(System.in);
        int km = scanner.nextInt();
        for (Plane plane : planes
                ) {
            plane.firstFly(km);

        }
        System.out.println();
        return planes;
    }

    private static void sortMaxDistance(final Airport airport) {
        airport.sortByMaxDistance();
    }

    private static void checkLoadCapacity(final Airport airport) {
        System.out.println("Load capacity = " + airport.countLoadCapacity()
                + " kg.");
    }

    private static void checkNumberOfSeats(final Airport airport) {
        System.out.println("Number of seats  = " + airport.countSeats());
    }

    private static void findByFilter(final Airport airport) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Set from fuel :");
        double from = scanner.nextDouble();
        System.out.println("and to fuel :");
        double to = scanner.nextDouble();

        airport.getByFuelFilter(from, to);
    }
}

