package com.vytrykhovskyi.planes;

public class PassengerPlane extends Plane {
    private int rowOfSeats;

    public PassengerPlane(final String mark, final double weight,
                          final int maxFly, final int loadCapacity,
                          final int seatsCount, final int rowOfSeat) {
        super(mark, weight, maxFly, loadCapacity, seatsCount);
        this.rowOfSeats = rowOfSeat;
    }

    public final void firstFly(final int km) {
        if (getMaxDistance() < km) {
            System.out.println("Sorry, " + getMark() + " does`nt fly so far");
            setFuelUsing(0.0);
        } else {
            System.out.println(getMark() + " fly " + km + " km...");
            double fuelUsage = km * (getWeight() + getMaxDistance()
                    + getLoadCapacity()) / rowOfSeats * Math.random() / 1000;
            setFuelUsing(fuelUsage);
        }
    }

    @Override
    public final String toString() {
        return super.toString()
                + ", rowOfSeats=" + rowOfSeats
                + '}';
    }
}
