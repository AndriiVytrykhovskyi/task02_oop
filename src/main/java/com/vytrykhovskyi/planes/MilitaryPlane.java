package com.vytrykhovskyi.planes;

public class MilitaryPlane extends Plane {
    private boolean bombWeapon;

    public MilitaryPlane(final String mark, final double weight,
                         final int maxFly, final int loadCapacity,
                         final int seatsCount, final boolean bombWp) {
        super(mark, weight, maxFly, loadCapacity, seatsCount);
        this.bombWeapon = bombWp;
    }

    public final void firstFly(final int km) {
        if (getMaxDistance() < km) {
            System.out.println("Sorry, " + getMark() + " does`nt fly so far");
            setFuelUsing(0.0);
        } else {
            System.out.println(getMark() + " fly " + km + " km...");
            double fuelUsage = 0.0;
            if (bombWeapon) {
                fuelUsage = km * (getWeight() + getLoadCapacity()
                        + getMaxDistance()) * Math.random() / 1000;
            } else {
                fuelUsage = 2 * km * (getLoadCapacity()
                        + getMaxDistance()
                        + getWeight()) * Math.random() / 1000;

            }
            setFuelUsing(fuelUsage);
        }
    }

    @Override
    public final String toString() {
        return super.toString()
                + ", bombWeapon=" + bombWeapon
                + '}';
    }
}
