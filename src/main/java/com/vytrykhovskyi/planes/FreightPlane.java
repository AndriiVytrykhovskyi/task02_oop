package com.vytrykhovskyi.planes;

public class FreightPlane extends Plane {
    private int freightSquare;

    public FreightPlane(final String mark, final double weight,
                        final int maxFly, final int loadCapacity,
                        final int seatsCount, final int freightSqr) {
        super(mark, weight, maxFly, loadCapacity, seatsCount);
        this.freightSquare = freightSqr;
    }

    public final void firstFly(final int km) {
        if (getMaxDistance() < km) {
            System.out.println("Sorry, " + getMark() + " does`nt fly so far");
            setFuelUsing(0.0);
        } else {
            System.out.println(getMark() + " fly " + km + " km...");
            double fuelUsage = km * (getWeight() + getMaxDistance()
                    + getLoadCapacity() * Math.random()) / 1000;
            setFuelUsing(fuelUsage);
        }
    }

    @Override
    public final String toString() {
        return super.toString()
                + ", freightSquare=" + freightSquare
                + '}';
    }
}
