package com.vytrykhovskyi.planes;

public abstract class Plane {
    private String mark;
    private double weight;
    private int maxDistance;
    private int loadCapacity;
    private double fuelUsing;
    private int seatsCount;

    public Plane(final String m, final double w,
                 final int maxD, final int loadC, final int seatsC) {
        this.mark = m;
        this.weight = w;
        this.maxDistance = maxD;
        this.loadCapacity = loadC;
        this.seatsCount = seatsC;
    }

    public abstract void firstFly(int km);


    public final String getMark() {
        return mark;
    }

    public final double getWeight() {
        return weight;
    }

    public final int getMaxDistance() {
        return maxDistance;
    }

    public final int getSeatsCount() {
        return seatsCount;
    }

    public final int getLoadCapacity() {
        return loadCapacity;
    }

    public final double getFuelUsing() {
        return fuelUsing;
    }

    public final void setFuelUsing(final double fuelUs) {
        this.fuelUsing = fuelUs;
    }

    @Override
    public String toString() {
        return "Plane{"
                + "mark='" + mark + '\''
                + ", weight=" + weight
                + ", maxDistance=" + maxDistance
                + ", loadCapacity=" + loadCapacity
                + ", fuelUsing=" + fuelUsing
                + ", seatsCount=" + seatsCount;
    }
}



