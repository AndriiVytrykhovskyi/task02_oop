package com.vytrykhovskyi;


import com.vytrykhovskyi.planes.Plane;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Airport {

    private List<Plane> planes = new ArrayList<>();

    public Airport(final List<Plane> plane) {
        this.planes = plane;
    }

    public final List<Plane> getPlanes() {
        return planes;
    }

    public final void sortByMaxDistance() {
        Comparator<Plane> cmp = Comparator.comparing(Plane::getMaxDistance);
        List<Plane> collect = planes.stream()
                .sorted(cmp)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);

    }

    public final int countLoadCapacity() {
        return planes.stream()
                .mapToInt(Plane::getLoadCapacity)
                .sum();
    }

    public final int countSeats() {
        return planes.stream()
                .mapToInt(Plane::getSeatsCount)
                .sum();
    }

    public final void getByFuelFilter(final double from, final double to) {
        List<Plane> collect = planes.stream()
                .filter((p) -> p.getFuelUsing()
                        >= from && p.getFuelUsing() <= to)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
    }
}
